[![](https://img.shields.io/badge/Maintained%3F-yes-green.svg)]()

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

[![Yaydoo logo](https://gist.githubusercontent.com/viveralia/cc3c7e0d470f14cfc5d5612b2afffa44/raw/76d68629c37a5677760f2e410f6235753c4b0845/yaydoo.svg)](https://yaydoo.com/)

# React CSR Boilerplate

A highly opinionated boilerplate to kickstart the next Yaydoo Product 
🦄

This template relies on client side rendering (CSR) only, so use it wisely. 

**Quick tip:** SEO must not be a crucial part of the product to use this boilerplate as it is.

## Features
- ⚡️ Lightning fast HMR and builds thanks to [Vite](https://vitejs.dev/)
- ⌨️ Type safety using [TypeScript](https://www.typescriptlang.org/) in strict mode
- 🌎 Internationalization support with [react-i18next](https://react.i18next.com/)
- 🃏 Testing support with [Jest](https://jestjs.io/) and [react-testing-library](https://testing-library.com/docs/react-testing-library/intro/)
- 🌈 [Prettier](https://prettier.io/) for consistent code style
- 📋 Standarized "best practices" extended from [Airbnb ESLint config](https://www.npmjs.com/package/eslint-config-airbnb)
- 📝 Standarized commits with commitizen and commitlint in the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) format
- 🪝 Code quality assurance thanks to [husky](https://github.com/typicode/husky) and [lint-staged](https://github.com/okonet/lint-staged)
- 🧮 Mock server CRUDs with [JSON server](https://github.com/typicode/json-server)
- 🖥 [VSCode](https://code.visualstudio.com/) first class support

## Architecture
As the product grows, features may come and go, thats why this project relies on a **module-like architecture**. Every feature should be encapsulated inside the `features` folder and treated as a micro app, in order to keep them loosely coupled with each other and preserve scalability and maintainability.

### Components structure
The UI components are separated into folders based on [Brad Frost's Atomic Design Principles](https://bradfrost.com/blog/post/atomic-web-design/). Think of it as an extension of the popular [Container - View pattern](https://medium.com/@learnreact/container-components-c0e67432e005), where **pages handle most of the complex logic** and the rest (atoms, molecules, organisms and templates) are merely presentational views.

## Developing
Install the dependencies with yarn:

```sh
$ yarn
```

Create a `db.json` file in the root, it will serve as a fake db for the mock server.

Start the vite dev server and the mock server by running:

```sh
$ yarn dev
```

## Committing
After adding a feature or making any changes, it is recommended that you use the commitizen CLI to commit those changes.

```sh
$ yarn cm
```

It will prompt you some questions about the changes made and run a local pipeline to ensure code quality across the codebase. The CLI will fix most of the problems, but it is also likely that you fix some others.

## Tips
This project works best by using VS Code.

Here are some plugins recommended to install and improve your DX:

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint): Enables lint fixing on save.