{
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": "./tsconfig.json",
    "ecmaVersion": "latest",
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "env": {
    "node": true,
    "browser": true
  },
  "settings": {
    "react": {
      "version": "detect"
    }
  },
  "extends": [
    "airbnb-typescript",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:prettier/recommended",
    "plugin:eslint-comments/recommended",
    "plugin:typescript-sort-keys/recommended",
    "plugin:react/recommended",
    "plugin:react/jsx-runtime",
    "plugin:jsx-a11y/recommended",
    "plugin:promise/recommended",
    "plugin:unicorn/recommended",
    "prettier"
  ],
  "plugins": [
    "import",
    "react",
    "@typescript-eslint",
    "eslint-plugin-import-helpers",
    "eslint-comments",
    "promise",
    "sort-keys-fix",
    "typescript-sort-keys",
    "unicorn"
  ],
  "overrides": [
    {
      "files": ["*.js"],
      "rules": {
        "@typescript-eslint/no-var-requires": "off"
      }
    }
  ],
  "rules": {
    "@typescript-eslint/no-non-null-assertion": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "import/extensions": ["off"],
    "import-helpers/order-imports": [
      "warn",
      {
        "newlinesBetween": "always",
        "groups": [
          "module",
          "/^~/",
          [
            "parent",
            "sibling",
            "index"
          ]
        ],
        "alphabetize": {
          "order": "asc",
          "ignoreCase": true
        }
      }
    ],
    "no-console": "error",
    "prettier/prettier": "warn",
    "sort-keys-fix/sort-keys-fix": "error",
    "unicorn/filename-case": [
      "error",
      {
        "case": "kebabCase",
        "ignore": [
          ".tsx$"
        ]
      }
    ],
    "unicorn/prevent-abbreviations": [
      "error",
      {
        "ignore": [
          "\\.e2e$",
          "^env.d$",
          "Props$"
        ]
      }
    ]
  }
}
