// Define env variables here for TypeScript intellisense
interface ImportMetaEnvironment {
  readonly DEV: boolean;
}

interface ImportMeta {
  readonly env: ImportMetaEnvironment;
}
