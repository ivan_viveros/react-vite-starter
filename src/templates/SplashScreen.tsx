import { FC } from "react";

import { ReactComponent as YaydooIsotypeColor } from "~icons/yaydoo-isotype-color.svg";

const SplashScreen: FC = () => {
  return <YaydooIsotypeColor />;
};

export default SplashScreen;
