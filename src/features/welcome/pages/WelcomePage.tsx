import { FC } from "react";
import { useTranslation } from "react-i18next";

import Seo from "~molecules/Seo";

const WelcomePage: FC = () => {
  const { t } = useTranslation("welcome");

  return (
    <main>
      <Seo title={t("pageTitle")} />
      <h1>{t("headline")}</h1>
      <p>{t("paragraph")}</p>
    </main>
  );
};

export default WelcomePage;
