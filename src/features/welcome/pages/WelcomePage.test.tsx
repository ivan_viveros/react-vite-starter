import { render } from "@testing-library/react";

import WelcomePage from "./WelcomePage";

it("renders correctly", () => {
  const { getByText } = render(<WelcomePage />);
  const welcomePage = getByText("headline");
  expect(welcomePage).toBeInTheDocument();
});
