import { FC } from "react";
import { Route, Routes } from "react-router-dom";

import { WelcomePage } from "../pages";

const WelcomeRoutes: FC = () => {
  return (
    <Routes>
      <Route path="/">
        <Route index element={<WelcomePage />} />
      </Route>
    </Routes>
  );
};

export default WelcomeRoutes;
