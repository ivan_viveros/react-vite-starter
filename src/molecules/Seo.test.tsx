import { render } from "@testing-library/react";
import * as faker from "faker";
import { Helmet } from "react-helmet";

import Seo from "./Seo";

it("should change the document title in the correct template format", () => {
  const randomTitle = faker.lorem.sentence();
  render(<Seo title={randomTitle} />);
  const helmet = Helmet.peek();
  expect(helmet.title).toBe(`${randomTitle} | Yaydoo`);
});
