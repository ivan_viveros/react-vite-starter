import { FC } from "react";
import { Helmet } from "react-helmet";

export interface SeoProps {
  title: string;
}

const Seo: FC<SeoProps> = ({ title }) => {
  return (
    <Helmet titleTemplate="%s | Yaydoo" defaultTitle="CSR React Starter">
      <title>{title}</title>
    </Helmet>
  );
};

export default Seo;
