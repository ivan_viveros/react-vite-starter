import i18n from "i18next";
import detector from "i18next-browser-languagedetector";
import backend from "i18next-http-backend";
import { initReactI18next } from "react-i18next";

export const supportedLngs = ["en", "es"];

// All translation files are located inside public/locales
void i18n
  .use(detector)
  .use(backend)
  .use(initReactI18next)
  .init({
    debug: import.meta.env.DEV,
    defaultNS: "app",
    fallbackLng: "en",
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
    ns: "app",
    supportedLngs,
  });

export { default } from "i18next";
