import React, { lazy, Suspense } from "react";
import ReactDOM from "react-dom";

import "~app/i18n";
import SplashScreen from "~templates/SplashScreen";

const App = lazy(() => import("./app/App"));

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<SplashScreen />}>
      <App />
    </Suspense>
  </React.StrictMode>,
  document.querySelector("#root")
);
